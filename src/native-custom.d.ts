declare module '@nativescript/capacitor' {
  export interface customNativeAPI extends nativeCustom {}
}

/**
 * Define your own custom strongly typed native helpers here.
 */
export interface nativeCustom {
  openNativeModalView: () => void;
}

export interface nativeCustom {
  setScreenBrightness: (value: number) => void;
}