import { Component, OnInit } from '@angular/core';
import { LocalNotifications } from '@capacitor/local-notifications';
import { native } from '@nativescript/capacitor';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  constructor() {}

  async ngOnInit(){
    await LocalNotifications.requestPermissions();
  }

  async OnClick(){
    await LocalNotifications.schedule({
      notifications: [
        {
          title: "Powiadomienie",
          body: "Hey!",
          id: 1
        }
      ]
    });
  }

  async OnClick2(){
    native.setScreenBrightness(.8);
  }

}
